chrome.runtime.onMessage.addListener(
  function(request, sender, sendResponse) {
    if( request.message === "clicked_browser_action" ) {
      if ($('#dog-pizza').length > 0) {
        $('#dog-pizza').remove();
        return false;
      }

      var dogImgURL = chrome.extension.getURL('images/dog.gif'),
          $wrapper = $('<div/>').attr('id', 'dog-pizza'),
          $img = $('<img/>').attr('src', dogImgURL);

      $wrapper.append($img).appendTo('body');

      chrome.runtime.sendMessage(
        {
          message: "show_data",
          data: "LLEGARON LAS PITZAASSSS!"
        }, function() {
          console.log("envie el mensaje al backend");
        }
      );
    }
  }
);
